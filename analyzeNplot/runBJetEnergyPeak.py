#!/usr/bin/env python

import optparse
import os,sys
import json
import pickle
import ROOT
from subprocess import Popen, PIPE


# Perform the analysis on a single file
def runBJetEnergyPeak(inFileURL, outFileURL, xsec = None):

    print '...analysing %s' % inFileURL

    # Book some histograms
    histos={
        'bjeten'   : ROOT.TH1F('bjeten', ';Energy [GeV]; Jets', 30, 0, 300),
        'bjetenls' : ROOT.TH1F('bjetenls', ';log(E);  1/E dN_{b jets}/dlog(E)', 20, 3.0, 7.0),
        'nvtx'     : ROOT.TH1F('nvtx', ';Vertex multiplicity; Events', 30, 0, 30),
        'nbtags'   : ROOT.TH1F('nbtags', ';b-tag multiplicity; Events', 5, 0, 5),

        # Add new histogram for number of jets
        'njets'    : ROOT.TH1F('njets', ';Jet multiplicity; Events', 12, 0, 12),

        # Add new histogram for number of leptons
        'nleptons' : ROOT.TH1F('nleptons', ';Lepton multiplicity; Events', 6, 0, 6),

        # Add new histogram for electron pt
        'elpt'     : ROOT.TH1F('elpt', ';Electron p_{T}; Events', 50, 0, 200),

        # Add new histogram for muon pt
        'mupt'     : ROOT.TH1F('mupt', ';Muon p_{T}; Events', 50, 0, 200),

        'lep1pt'   : ROOT.TH1F('lep1pt', ';Leading lepton p_{T}; Events', 50, 0, 200),
        'lep2pt'   : ROOT.TH1F('lep2pt', ';Sub-leading lepton p_{T}; Events', 50, 0, 200)
    }
    for key in histos:
        histos[key].Sumw2()
        histos[key].SetDirectory(0)

    # Open file and loop over events tree
    fIn = ROOT.TFile.Open(inFileURL)
    tree = fIn.Get('data')
    totalEntries = tree.GetEntriesFast()
    for i in xrange(0, totalEntries):

        tree.GetEntry(i)
        if i % 100 == 0: sys.stdout.write('\r [ %d/100 ] done' % (int(float(100.0 * i) / float(totalEntries))))
        # Require at least two jets
        nJets, nBtags, nLeptons = 0, 0, 0
        taggedJetsP4 = []
        for ij in xrange(0, tree.nJet):

            # Get the kinematics and select the jet
            jp4 = ROOT.TLorentzVector()
            jp4.SetPtEtaPhiM(tree.Jet_pt[ij], tree.Jet_eta[ij], tree.Jet_phi[ij], tree.Jet_mass[ij])
            if jp4.Pt() < 30 or ROOT.TMath.Abs(jp4.Eta()) > 2.4: continue

            # Count selected jet
            nJets += 1

            # Save P4 for b-tagged jet
            if tree.Jet_CombIVF[ij] > 0.8484: # Medium cut
                nBtags += 1
                taggedJetsP4.append(jp4)

        if nJets < 2: continue
        if nBtags != 1 and nBtags != 2: continue

        leptons = []
        leptonid = []
        for ij in xrange(0, tree.nLepton):
            # Get the kinematics and select the lepton
            lp4 = ROOT.TLorentzVector()
            lp4.SetPtEtaPhiM(tree.Lepton_pt[ij], tree.Lepton_eta[ij], tree.Lepton_phi[ij], 0)
            if lp4.Pt() < 20 or ROOT.TMath.Abs(lp4.Eta()) > 2.4: continue
            leptons.append(lp4)
            if tree.nGenWeight > 0: # for MC
                leptonid.append(tree.Lepton_gid[ij])
            else: # for data
                leptonid.append(tree.Lepton_id[ij])
            # Count selected leptons
            nLeptons +=1

        if nLeptons < 2: continue

        # Generator level weight only for MC
        evWgt = 1.0
        if xsec: evWgt = xsec * tree.LepSelEffWeights[0] * tree.PUWeights[0] # b jet weight?
        if tree.nGenWeight > 0: evWgt *= tree.GenWeights[0]

        # Ready to fill the histograms
        # Fill nvtx plot
        histos['nvtx'].Fill(tree.nPV, evWgt)

        # Fill nbtags plot
        histos['nbtags'].Fill(nBtags, evWgt)

        # Fill njets plot
        histos['njets'].Fill(nJets, evWgt)

        # Fill nleptons plot
        histos['nleptons'].Fill(nLeptons, evWgt)

        # Fill electron and muon plots
        for ilep, lep in enumerate(leptons):
            if abs(leptonid[ilep]) == 11:
                histos['elpt'].Fill(lep.Pt(), evWgt)
            elif abs(leptonid[ilep]) == 13:
                histos['mupt'].Fill(lep.Pt(), evWgt)

        # Fill leading and subleading lepton pt
        max1pt = -1.0
        for ilep, lep in enumerate(leptons):
            max1pt = max(max1pt, lep.Pt())
        max2pt = -1.0
        for ilep, lep in enumerate(leptons):
            if lep.Pt() > max2pt and lep.Pt() < max1pt:
                max2pt = lep.Pt()
        histos['lep1pt'].Fill(max1pt, evWgt)
        histos['lep2pt'].Fill(max2pt, evWgt)

        # Use up to two leading b-tagged jets
        for ij in xrange(0,len(taggedJetsP4)):
            if ij > 1: break
            histos['bjeten'].Fill(taggedJetsP4[ij].E(), evWgt)
            histos['bjetenls'].Fill(ROOT.TMath.Log(taggedJetsP4[ij].E()), evWgt / taggedJetsP4[ij].E())

    fIn.Close()

    # Save histograms to file
    fOut = ROOT.TFile.Open(outFileURL, 'RECREATE')
    for key in histos: histos[key].Write()
    fOut.Close()


# Wrapper to be used when run in parallel
def runBJetEnergyPeakPacked(args):

    try:
        return runBJetEnergyPeak(inFileURL = args[0], outFileURL = args[1], xsec = args[2])
    except :
        print 50 * '<'
        print "  Problem (%s) with %s continuing without" % (sys.exc_info()[1], args[0])
        print 50 * '<'
        return False


# Steer the script
def main():

    # Configuration and command line options
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-j', '--json', dest = 'json', help = 'json with list of files', default = None, type = 'string')
    parser.add_option('-i', '--inDir', dest = 'inDir', help = 'input directory with files', default = None, type = 'string')
    parser.add_option('-o', '--outDir', dest = 'outDir', help = 'output directory', default = 'analysis', type = 'string')
    parser.add_option('-n', '--njobs', dest = 'njobs', help = '# jobs to run in parallel', default = 0, type = 'int')
    (opt, args) = parser.parse_args()

    # Read list of samples
    jsonFile = open(opt.json, 'r')
    samplesList = json.load(jsonFile, encoding = 'utf-8').items()
    jsonFile.close()

    # Prepare output
    if len(opt.outDir) == 0: opt.outDir = './'
    os.system('mkdir -p %s' % opt.outDir)

    # Create the analysis jobs
    taskList = []
    for sample, sampleInfo in samplesList:
        # inFileURL = 'root://cmseos.fnal.gov//%s/%s.root' % (opt.inDir, sample)
        inFileURL = '%s/%s.root' % (opt.inDir, sample)
        if not os.path.isfile(inFileURL):
            print inFileURL, "does not exist! SKIPPING IT!"
            continue
        xsec = sampleInfo[0] if sampleInfo[1] == 0 else None
        outFileURL = '%s/%s.root' % (opt.outDir, sample)
        taskList.append((inFileURL, outFileURL, xsec))

    # Run the analysis jobs
    if opt.njobs == 0:
        for inFileURL, outFileURL, xsec in taskList:
            runBJetEnergyPeak(inFileURL = inFileURL, outFileURL = outFileURL, xsec = xsec)
    else:
        from multiprocessing import Pool
        pool = Pool(opt.njobs)
        pool.map(runBJetEnergyPeakPacked, taskList)

    # All done here
    print 'Analysis results are available in %s' % opt.outDir
    exit(0)


# For execution from another script
if __name__ == "__main__":
    sys.exit(main())
