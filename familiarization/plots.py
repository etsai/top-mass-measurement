#!/usr/bin/env python

# Run by calling in the shell:
# python plots.py

import ROOT
from ROOT import TFile, TCanvas


# Plot b-tagged jet transverse momentum for data
# Open the file
fiData = TFile.Open("/nfs/dust/cms/group/cmsdas2023/long-ex-top/MuonEG_2016Hv2.root", "READ")
# Get the tree
treeData = fiData.Get("data")
# Make a new TCanvas
c1 = TCanvas("canvas 1", "c1", 600, 600)
c1.cd()
# Draw the variable
treeData.Draw("Jet_pt")
treeData.Draw("Jet_pt", "Jet_CombIVF>0.605", "e1same") # b-jets in data
# Save the plot
# c1.SaveAs("plotData.png")
c1.SaveAs("plotData.pdf")


# Plot b jet generated transverse momentum for MC
# Open the file
fiMC = TFile.Open("/nfs/dust/cms/group/cmsdas2023/long-ex-top/TTJets.root", "READ")
# Get the tree
treeMC = fiMC.Get("data")
# Make a new TCanvas
c2 = TCanvas("canvas 2", "c2", 600, 600)
c2.cd()
# Draw the variable
treeMC.Draw("Jet_genpt")
treeMC.Draw("Jet_genpt", "abs(Jet_flavour)==5", "e1same") # b-jets in MC
# Save the plot
# c2.SaveAs("plotMC.png")
c2.SaveAs("plotMC.pdf")


# Plot resolution for b-jet pt
c3 = TCanvas("canvas 3", "c3", 600, 600)
c3.cd()
treeMC.Draw("(Jet_pt-Jet_genpt)/Jet_genpt","abs(Jet_flavour)==5 && Jet_genpt > 0")
c3.SaveAs("plotRes.pdf")


# Plot pt of genuine and reconstructed electrons
c4 = TCanvas("canvas 4", "c4", 600, 600)
c4.cd()
treeMC.Draw("Lepton_pt","abs(Lepton_id)==11 && Lepton_pt < 200")
treeMC.Draw("Lepton_pt","abs(Lepton_gid)==11 && Lepton_pt < 200", "e1same")
c4.SaveAs("plotElPt.pdf")


# Plot pt of genuine and reconstructed electrons
c5 = TCanvas("canvas 5", "c5", 600, 600)
c5.cd()
treeMC.Draw("Lepton_pt","abs(Lepton_id)==13 && Lepton_pt < 200")
treeMC.Draw("Lepton_pt","abs(Lepton_gid)==13 && Lepton_pt < 200", "e1same")
c5.SaveAs("plotMuPt.pdf")
